from tqdm import tqdm
import numpy as np

top = 100*1000


def is_prime(n):
    if n == 2:
        return True
    for factor in range(2, int(np.sqrt(n)) + 2):
        if n % factor == 0:
            return False
    return True


primes = [x for x in tqdm(range(2, top)) if is_prime(x)]
primes_set = set(primes)
# print('num primes below %d:' % top, len(primes))


def prime_factors_of(n):
    max_factor = min(int(n / 2) + 1, n)
    for p in primes:
        if p >= max_factor:
            return
        elif n % p == 0:
            yield p
    return


def rad(n):
    # product of distinct prime factors of n
    prod = 1

    for p in prime_factors_of(n):
        prod *= p
    if n in primes_set:
        prod *= n

    return prod


num_rad_pairs = [(n, rad(n)) for n in tqdm(range(1, top+1))]
print('first:', num_rad_pairs[:5])
print('last:', num_rad_pairs[-5:])
print('10000th value', num_rad_pairs[9999])
print()

sorted_pairs = sorted(num_rad_pairs, key=lambda x: x[1])
print('sorted_first:', sorted_pairs[:5])
print('sorted_last:', sorted_pairs[-5:])
print()

print(sorted_pairs[:100])
print()

answer = sorted_pairs[9999]
print('answer:', answer[0], 'rad:', answer[1])