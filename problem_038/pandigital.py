def get_pandigital(n):
	concat_sum = ''
	for multiplier in range(1, 10):
		new_product = str(n * multiplier)
		if '0' not in new_product and all([c not in new_product[i+1:] for i, c in enumerate(new_product)]) and all([c not in concat_sum for c in new_product]):
			concat_sum += new_product
		else:
			return None

		if len(concat_sum) == 9:
			return concat_sum

for i in range(10000):
	pandigital = get_pandigital(i)
	if pandigital is not None:
		print i, pandigital
