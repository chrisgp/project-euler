with open('roman.txt') as f:
    romans_in = [x for x in f.read().split('\n') if len(x) > 0]

# print(romans_in)

count = 0
reduction = 0
for r in romans_in:
    if 'CCCC' in r or 'XXXX' in r or 'IIII' in r:
        count += 1
        r_min = r
        if 'IIII' in r:
            if 'VIIII' in r:
                r_min = r_min.replace('VIIII', 'IX')
            else: # 'IIII' in r:
                r_min = r_min.replace('IIII', 'IV')
        if 'XXXX' in r:
            if 'LXXXX' in r:
                r_min = r_min.replace('LXXXX', 'XC')
            else:
                r_min = r_min.replace('XXXX', 'XL')
        if 'CCCC' in r:
            if 'DCCCC' in r:
                r_min = r_min.replace('DCCCC', 'CM')
            else:
                r_min = r_min.replace('CCCC', 'CD')
        print(count, r, r_min)
        reduction += len(r) - len(r_min)

print(reduction)
