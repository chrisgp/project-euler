import numpy as np

def is_prime(n):
	for factor in range(2, int(np.sqrt(n)) + 2):
		if n % factor == 0:
			return False
	return True

four_digit_primes = [x for x in np.arange(1000, 10000) if is_prime(x)]
prime_hashes = [''.join(sorted(str(x))) for x in four_digit_primes]
table = {}
for p, h in zip(four_digit_primes, prime_hashes):
	if h not in table:
		table[h] = []
	table[h].append(p)

for key in table.keys():
	permutations = table[key]
	if len(permutations) >= 3:
		for i, x in enumerate(permutations):
			for j, y in enumerate(permutations[i+1:]):
				z = 2*y-x
				if z in permutations:
					print x, y, z
