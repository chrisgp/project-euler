
bases, exponents = [], []
with open('p099_base_exp.txt') as f:
    lines = f.read().split('\n')
    for l in lines:
        base, exp = l.split(',')
        bases.append(int(base))
        exponents.append(int(exp))

print(exponents)
print(min(exponents))

test_values = []
max_val, max_line_no = -1, -1
for b, e in zip(bases, exponents):
    test_values.append(b ** (e / min(exponents)))
    print(test_values[-1])
    if test_values[-1] > max_val:
        max_val = test_values[-1]
        max_line_no = len(test_values) - 1
print(max_line_no + 1, max_val)