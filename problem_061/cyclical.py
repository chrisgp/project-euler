

def four_digit_polgyons(n):
    if n == 3:
        ls = [x * (x + 1) // 2 for x in range(142)]
    elif n == 4:
        ls = [x * x for x in range(142)]
    elif n == 5:
        ls = [x * (3 * x - 1) // 2 for x in range(142)]
    elif n == 6:
        ls = [x * (2 * x - 1) for x in range(142)]
    elif n == 7:
        ls = [x * (5 * x - 3) // 2 for x in range(142)]
    elif n == 8:
        ls = [x * (3 * x - 2) for x in range(142)]
    else:
        assert False
    ls = [x for x in ls if 1000 <= x < 10000]
    return ls


seen_cycles = set()


def recursive_find_cycles(this_cycle_so_far, starts_dict, figurates_by_num):
    # if the partial cycle is too long, stop
    if len(this_cycle_so_far) > 7:
        return []

    # if we have a full cycle, stop
    if len(this_cycle_so_far) > 1 and this_cycle_so_far[0] == this_cycle_so_far[-1]:
        lineup_hash = str(sorted(this_cycle_so_far[:-1]))
        if len(this_cycle_so_far) == 7 and lineup_hash not in seen_cycles:  # need a len 6 cycle, plus the repeated first
            figurates_represented = [figurates_by_num[n] for n in this_cycle_so_far[:-1]]
            if len(set.union(*figurates_represented)) == 6:
                print(this_cycle_so_far[:-1], sum(this_cycle_so_far[:-1]), figurates_represented)
                seen_cycles.add(lineup_hash)
                return [this_cycle_so_far]
            else:
                return []
        else:
            return []

    # if we don't have a full cycle, go through every possible next step
    full_cycles = []
    this_end = this_cycle_so_far[-1] % 100
    if this_end in starts_dict:
        possible_next_ls = starts_dict[this_end]
        for possible_next in possible_next_ls:
            full_cycles += recursive_find_cycles(this_cycle_so_far + [possible_next], starts_dict, figurates_by_num)
    return full_cycles


def recursive_start(nums, starts_dict, figurates_by_num):
    all_cycles = []
    for n in nums:
        all_cycles += recursive_find_cycles([n], starts_dict, figurates_by_num)
    return all_cycles


def main():
    npolys = [3, 4, 5, 6, 7, 8]
    figurate_ls_ls = [four_digit_polgyons(n) for n in npolys]

    figurates_by_num = {}
    for n, figurate_ls in zip(npolys, figurate_ls_ls):
        for x in figurate_ls:
            if x not in figurates_by_num:
                figurates_by_num[x] = set()
            figurates_by_num[x].add(n)

    num_starts = {}
    num_ends = {}
    for n in figurates_by_num.keys():
        start = n // 100
        end = n % 100
        if start not in num_starts:
            num_starts[start] = []
        if end not in num_ends:
            num_ends[end] = []
        num_starts[start].append(n)
        num_ends[end].append(n)

    nums = [n for n in figurates_by_num.keys() if (n % 100) in num_starts and (n // 100) in num_ends]

    cycles = recursive_start(nums, num_starts, figurates_by_num)

    # correct answer:
    # 28684 is the sum
    # [8128, 2882, 8256, 5625, 2512, 1281] 28684 [{3, 6}, {5}, {3}, {4}, {7}, {8}]

    print()



if '__main__' == __name__:
    main()
