valid_char_min = 32
valid_char_max = 126

lowercase_min = 97
lowercase_max = 122

with open('p059_cipher.txt') as f:
	encrypted = map(int, f.read().split(','))


# key is stored as a list of 3 ints
def key_gives_only_printable(key):
	for i, c in enumerate(encrypted):
		decrypted_char = c ^ key[i % 3]
		if decrypted_char < valid_char_min or decrypted_char > valid_char_max:
			return False
	return True


def decrypted(key):
	new_chars = []
	for i, c in enumerate(encrypted):
		decrypted_char = c ^ key[i % 3]
		new_chars.append(decrypted_char)
	return ''.join(map(chr, new_chars))


for a in range(lowercase_min, lowercase_max + 1):
	for b in range(lowercase_min, lowercase_max + 1):
		for c in range(lowercase_min, lowercase_max + 1):
			if key_gives_only_printable([a,b,c]):
				print '\n\n'
				print ''.join(map(chr, [a,b,c]))
				print decrypted([a,b,c])

print '\nanswer:'
print sum([ord(c) for c in decrypted(map(ord, ['e','x','p']))])