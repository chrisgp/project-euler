import numpy as np


def rotation(n):
    shift_right = n // 10
    num_digits = np.ceil(np.log10(n))
    rotated = int((n % 10)*10**(num_digits - 1)) + shift_right
    return rotated


def rotation_is_divisor(n):
    rotated = rotation(n)
    if rotated % n == 0:
        return True
    return False


s = 0
for n in range(10, 1000001):
    if rotation_is_divisor(n) and n % 100 != (n % 10) * 11:
        s += n
        print(n, 'sum:', s)


for num_digits in range(2, 101):
    capped_digits = min(num_digits, 5)
    ones_unit = int('1' * capped_digits)
    for i in range(1, 10):
        s += ones_unit * i

print(s)