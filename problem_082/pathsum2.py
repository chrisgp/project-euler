import numpy as np
import pandas as pd
from tqdm import tqdm


def main():
    mat_df = pd.read_csv('p082_matrix.txt')
    mat = mat_df.values

    dp = np.zeros(mat.shape)
    dp[:, -1] = mat[:, -1]

    for col in tqdm(range(mat.shape[1] - 2, 0, -1)):
        # compute next leftward best minimum
        # assume this is for the first time we're in this column,
        # that means we don't have to worry about arriving at a cell in multiple ways
        for row in range(mat.shape[0]):
            min_sum = 9999999999
            # now loop over all moves up, and moves down, and see what the min sum is out of all of these
            for move_to_row in range(mat.shape[0]):
                if move_to_row < row:
                    col_cost = np.sum(mat[move_to_row:row + 1, col])
                elif move_to_row > row:
                    col_cost = np.sum(mat[row:move_to_row + 1, col])
                else:
                    col_cost = mat[row, col]

                if col_cost > min_sum:
                    break

                cost = col_cost + dp[move_to_row, col + 1]
                if cost < min_sum:
                    min_sum = cost
            dp[row, col] = min_sum

    overall_min_sum = 9999999999
    for row in range(mat.shape[0]):
        cost = mat[row, 0] + dp[row, 1]
        if cost < overall_min_sum:
            overall_min_sum = cost
    print('min sum:', int(overall_min_sum))


if '__main__' == __name__:
    main()
