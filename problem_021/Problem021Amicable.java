/* Chris Piller
*  Problem021Amicable.java */

public class Problem021Amicable {

   private static int d(int n) {
      int sum = 0;
      for (int i = 1; i < n; i++) {
         if (n % i == 0) {
            sum += i;
         }
      }
      return sum;
   }

   private static boolean isAmicable(int n) {
      if (n == d(d(n))) {
         return true;
      }
      return false;
   }
   
   public static void main(String[] args) {
      final int MAX = 10000;
      int sum = 0;
      
      for (int i = 0; i < MAX; i++) {
         if (isAmicable(i) && d(i) != i) {
            sum += i;
         }
      }
      
      // Testing the example case
      // Should print d(220): 284, d(284): 220 and true
      System.out.println("d(220): " + d(220) + '\t' + "d(284): " + d(284));
      System.out.println(isAmicable(220) + "\n\n");
      
      // Solution
      System.out.println("Solution: "+sum);
   }
}
