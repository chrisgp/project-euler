EPS = .0000001

def points_contain_0(side, other):
	if abs(side) < EPS or abs(other) < EPS:
		return True
	return side * other < -EPS


def contains_origin(coords_ls):
	# for each side: the side and the parrallel line through the left-out point needs to contain 0 
	# (intercepts have opposite signs or one intercept is 0)
	# be careful for the vertical line case
	for side in range(3):
		ax, ay, bx, by, cx, cy = [coords_ls[(side*2 + i) % 6] for i in range(6)]
		if ax == bx:
			# vertical line case
			side_intercept = ax
			other_point_intercept = cx
		else:
			rise = ay - by
			run = ax - bx
			side_intercept = ay - (rise * 1. / run) * ax
			other_point_intercept = cy - (rise * 1. / run) * cx

		# true for our AND if one of the intercepts is 0 or if the intercepts have different signs
		if not points_contain_0(side_intercept, other_point_intercept):
			return False

	return True


num_containing_origin = 0
with open('p102_triangles.txt') as f:
	lines = f.read().split('\n')
	for l in lines:
		if len(l) < 2:
			continue
		if contains_origin(map(int, l.split(','))):
			num_containing_origin += 1

print num_containing_origin