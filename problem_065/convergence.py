from fractions import Fraction

def denom_term_sequence(n):
	# for e
	assert n > 2

	seq = [2]

	i = 0
	while i + 1 < n:
		if i % 3 == 0:
			seq.append(1)
		elif i % 3 == 1:
			seq.append( ((i // 3) + 1) * 2 )
		elif i % 3 == 2:
			seq.append(1)

		i += 1

	return seq

def nth_convergent(n):
	seq = denom_term_sequence(n)

	conv = seq[-1]
	for i in range(n - 2, -1, -1):
		conv = seq[i] + Fraction(1, conv)

	return conv


def digit_sum(x):
	total = 0
	while x > 0:
		total += x % 10
		x /= 10

	return total

n = 100
print denom_term_sequence(n)
nth_conv = nth_convergent(n)
answer = digit_sum(nth_conv.numerator)



print answer
