import math
from time import time

start = time()

def is_perfect_square(n):
	if n <= 0:
		return False
	return int(math.sqrt(n))**2 == n

def is_int(n):
	return int(n) == n

def pentagonal_at(n):
	return int(n * (3*n - 1) / 2)

top = 5000
pentagonal = [n * (3*n-1) / 2 for n in range(1, top)]
pentagonal_set = set(pentagonal)

should_end = False
for target in pentagonal:
	for k in range(1, top):
		partial = target - k*(3*k-1)/2

		if partial <= 0:
			break
		elif partial % 3 == 0:
			partial /= 3
			if partial % k == 0:
				n = int(partial / k)
				lower = pentagonal_at(n)
				upper = pentagonal_at(n+k)
				if lower + upper in pentagonal_set:
					print()
					print('n:', n, 'k:', k)
					print('lower:', lower, 'upper:', upper, 'sum:', lower+upper, 'diff:', upper - lower)
					should_end = True
					break
	if should_end:
		break

print('%.2fs' % (time() - start))