import numpy as np


def is_prime(n):
    if n == 2:
        return True
    for factor in range(2, int(np.sqrt(n)) + 2):
        if n % factor == 0:
            return False
    return True


def eight_prime_with_n_digits(n):
    primes = [x for x in range(10 ** (n-1), 10 ** n) if is_prime(x)]
    # keys are masks like 10011 (where 1s are replaced digits) to decimal then values are lists of primes that match
    masks = {}

    for p in primes:
        pass






    print(primes)


def main():
    eight_prime_with_n_digits(5)


if '__main__' == __name__:
    main()
