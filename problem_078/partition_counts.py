from tqdm import tqdm

cache = {(1, 1): 1, (2, 2): 2, (3, 3): 3}
TOP = 1000000


def sum_count_recursive(n, max_allowed_term):
    # num ways to add to n with at least one term
    global cache

    if (n, max_allowed_term) in cache:
        return cache[(n, max_allowed_term)]

    if n == 1 or max_allowed_term == 1:
        # print((n, max_allowed_term), 1)
        cache[(n, max_allowed_term)] = 1
        return 1

    num_ways = 0
    for term in range(1, min(max_allowed_term + 1, n + 1)):
        if n - term > 0:
            num_ways += sum_count_recursive(n - term, min(max_allowed_term, n - term))
        else:
            num_ways += 1

    # print((n, max_allowed_term), num_ways)
    cache[(n, max_allowed_term)] = num_ways

    return num_ways


for n in tqdm(range(1, TOP)):
    ways = sum_count_recursive(n, n)
    if ways % 1000000 == 0:
        print(n, ways)
        break
print(cache)