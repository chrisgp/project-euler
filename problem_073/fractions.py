import numpy as np
from tqdm import tqdm


def is_prime(n):
    if n == 2:
        return True
    for factor in range(2, int(np.sqrt(n)) + 2):
        if n % factor == 0:
            return False
    return True


def prime_factors_of(n):
    for p in primes:
        if p > n:
            return
        elif n % p == 0:
            yield p
    return


top = 12*1000
primes = [x for x in tqdm(range(2, top)) if is_prime(x)]

total_fractions_between = 0
for d in range(4, top+1):
    # count number of reduced proper fractions between 1/3 and 1/2 non-inclusive

    prime_factors = [p for p in prime_factors_of(d)]

    min_num, max_num = int(np.floor(d/3)) + 1, int(np.ceil(d/2)) - 1
    for n in range(min_num, max_num + 1):
        is_coprime = True
        for p in prime_factors:
            if n % p == 0:
                is_coprime = False
                break
        if is_coprime:
            total_fractions_between += 1

    if d % 100 == 0:
        print(d, total_fractions_between)

