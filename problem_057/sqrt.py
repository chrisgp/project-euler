import numpy as np
from fractions import Fraction


def next_iteration(prev):
    return Fraction(1) + Fraction(1, 1+prev)


def num_digits(n):
    return len(str(n))


first_iteration = Fraction(1) + Fraction(1,2)

# print(first_iteration)
# print(next_iteration(first_iteration))
# print(next_iteration(next_iteration(first_iteration)))
# print(next_iteration(next_iteration(next_iteration(first_iteration))))
# print(next_iteration(next_iteration(next_iteration(next_iteration(first_iteration)))))

current_iteration = Fraction(1) + Fraction(1,2)
iter_num = 1
more_num_digits_count = 0
while iter_num <= 1000:
    current_iteration = next_iteration(current_iteration)
    if num_digits(current_iteration.numerator) > num_digits(current_iteration.denominator):
        more_num_digits_count += 1
    iter_num += 1

print(more_num_digits_count)