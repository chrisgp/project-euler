from tqdm import tqdm
import numpy as np
from fractions import Fraction

target_upper_bound = Fraction(15499, 94744)


def is_prime(n):
    if n == 2:
        return True
    for factor in range(2, int(np.sqrt(n)) + 2):
        if n % factor == 0:
            return False
    return True


top = 1000*1000*1000
primes = [x for x in tqdm(range(2, int(np.sqrt(top))+2)) if is_prime(x)]
print('num primes below %d:' % top, len(primes))


def prime_factors_of(n):
    max_factor = min(int(np.sqrt(n)) + 1, n)
    for p in primes:
        if p >= max_factor:
            return
        elif n % p == 0:
            yield p
    return


def resilient_frac(n):
    # number of numbers from 1 to n-1 that are coprime with n, divided by n-1
    totient_product = Fraction(n, 1)
    for p in prime_factors_of(n):
        totient_product *= Fraction(p - 1, p)

    return Fraction(totient_product, n-1)


min_frac = Fraction(1, 2)
divisor = 2*3*5*7*11*13*17*19*23
print(divisor)
for n in tqdm(range(divisor, top, divisor)):
    r_frac = resilient_frac(n)
    if r_frac < target_upper_bound:
        min_frac = r_frac
        print('new_min:', min_frac, 'dec: %.13f' % float(min_frac), ' at:', n)
        print('target: ', target_upper_bound, 'target dec: %.13f' % float(target_upper_bound))
        print('result:', n, r_frac)
        break
    elif r_frac < min_frac:
        min_frac = r_frac
        print('new_min:', min_frac, 'dec: %.3f' % float(min_frac), ' at:', n)
        print([p for p in prime_factors_of(n)])
        print()


# min_frac = Fraction(1,1)
# totient_product = Fraction(1)
# min_product = 1
# for i, p in enumerate(primes):
#     totient_product *= Fraction(p-1, p)
#     min_product *= p