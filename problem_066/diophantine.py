# import numba
from math import sqrt


# @numba.jit(nopython=True)
# def find_minimal_solution(D):
#     for x in range(1, 1000000):
#         for y in range(1, 1000000):
#             val = x * x - D * y * y
#             if val == 1:
#                 return x
#             elif val <= 0:
#                 break
#     return -1

# top = int(10 ** 9)
top = int(10 ** 6)


def find_minimal_solution(D):
    for x in range(2, top):
        y2 = (x * x - 1) / D
        y = int(sqrt(y2))
        if (x * x - 1) / D == y * y:
            return x
    return -1


max_minimal_x = 0
max_D_for_minimal_x = 0
squares = [z * z for z in range(1, 40)]
contenders = []
for D in range(1, 1000 + 1):
    if D in squares:
        continue

    x = find_minimal_solution(D)
    if x < 0:
        print(D)
        contenders.append(D)
    # if x > max_minimal_x:
    #     max_minimal_x = x
    #     max_D_for_minimal_x = D
    # print('D, x:', D, x, 'max D, x:', max_D_for_minimal_x, max_minimal_x)

print(contenders)