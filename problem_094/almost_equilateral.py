from math import sqrt, ceil
from tqdm import tqdm
import numpy as np
import numba

max_perimeter = 1000 * 1000 * 1000


@numba.jit()
def fast_perim_sum():
    perimeter_sum = 0
    for a in range(2, 20 + max_perimeter // 3):
        sqrt_gt = sqrt((4 * a * a - (a + 1) * (a + 1)) / 16)
        if np.ceil(sqrt_gt) == sqrt_gt:
            perimeter_sum += int(sqrt_gt * 3 + 1)
            print(a, a, a+1)

        sqrt_lt = sqrt((4 * a * a - (a - 1) * (a - 1)) / 16)
        if np.ceil(sqrt_lt) == sqrt_lt:
            perimeter_sum += int(sqrt_lt * 3 - 1)
            print(a, a, a-1)
    return perimeter_sum


print('perim sum:', fast_perim_sum())
