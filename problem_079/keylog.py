import numpy as np

def valid_code(full_pw, login):
    if login[0] not in full_pw or login[1] not in full_pw or login[2] not in full_pw:
        return False

    after_first = full_pw[full_pw.index(login[0])+1:]
    if login[1] not in after_first or login[2] not in after_first:
        return False

    after_second = after_first[after_first.index(login[1])+1:]
    if login[2] not in after_second:
        return False

    return True


with open('keylog.txt') as f:
    logins = [l for l in f.read().split('\n') if len(l) > 0]

logins = np.unique(logins)
print(logins, len(logins))

# print(np.any(['7' in s[1:] for s in logins]))
# print(np.any(['0' in s[:-1] for s in logins]))

print()
any_invalid = False
for l in logins:
    if not valid_code('73162890', l):
        print(l)
if not any_invalid:
    print('All Match')