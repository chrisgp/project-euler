public class Problem018PathSum {

    // StdIn must be the triangle data set,
    // with one row per line and each entry with one space following.
    // This solution uses the StdLib library to read and print values.
    // StdLib comes from Sedgewick and Wayne's Algorithms textbook.

    public static void main(String[] args) {
        final int NUM_ROWS = 15;

        int[][] triangle = new int[NUM_ROWS][NUM_ROWS];
        int[][] maxValueBottomUp = new int[NUM_ROWS][NUM_ROWS];

        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col <= row; col++)
                triangle[row][col] = StdIn.readInt();
            for (int col = row + 1; col < NUM_ROWS; col++)
                triangle[row][col] = 0;
        }

        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col < NUM_ROWS; col++) {
                StdOut.print(triangle[row][col] + "\t");
            }
            StdOut.println();
        }

        for (int i = 0; i < NUM_ROWS; i++)
            maxValueBottomUp[NUM_ROWS - 1][i] = triangle[NUM_ROWS - 1][i];

        for (int row = NUM_ROWS - 2; row >= 0; row--){
            for (int col = 0; col <= row; col++){
                maxValueBottomUp[row][col] = triangle[row][col] 
                    + Math.max(maxValueBottomUp[row + 1][col], 
                    maxValueBottomUp[row + 1][col + 1]);
            }
        } 

        StdOut.println("\n\n");
        for (int row = 0; row < NUM_ROWS; row++) {
            for (int col = 0; col < NUM_ROWS; col++) {
                StdOut.print(maxValueBottomUp[row][col] + "\t");
            }
            StdOut.println();
        }

        StdOut.println("\nAnswer: " + maxValueBottomUp[0][0]);
    }
}