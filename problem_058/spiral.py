import numpy as np
from tqdm import tqdm

def is_prime(n):
    if n == 2:
        return True
    for factor in range(2, int(np.sqrt(n)) + 2):
        if n % factor == 0:
            return False
    return True


def main():
    last_square = 1
    num_diag_primes, num_diag = 0, 1
    for side_length in tqdm(range(3, 100000, 2)):
        n = last_square
        for side in range(4):
            n += side_length - 1
            if is_prime(n):
                num_diag_primes += 1
        num_diag += 4
        last_square = n

        ratio = num_diag_primes / num_diag
        print(last_square, ':', num_diag_primes, num_diag, ratio)

        if ratio < .1:
            print(side_length)
            break


if '__main__' == __name__:
    main()
