import numpy as np
from tqdm import tqdm


def is_prime(n):
	if n == 2:
		return True
	for factor in range(2, int(np.sqrt(n)) + 2):
		if n % factor == 0:
			return False
	return True

# top = 1000
top = 1000000
primes = [x for x in tqdm(range(2, top)) if is_prime(x)]
primes_set = set(primes)
print(len(primes))

# loop over all prime indices as starting points
# find the maximum number of primes after that point that we can sum and still be <top
# go highest to lowest from that point and find the longest prime sum that sums to a prime
# save the max length and max length sum at every point
max_length = 1
max_length_sum = -1
for prime_i in range(len(primes)):
	for length in range(max_length + 1, len(primes) - prime_i):
		s = sum(primes[prime_i:(prime_i + length)])
		if s >= top:
			break
		if s in primes_set:
			max_length = length
			max_length_sum = s
			print(prime_i, max_length, max_length_sum)


# primes_below_1k = [x for x in np.arange(2, 1000) if is_prime(x)]
# print sum(primes_below_1k[:21])