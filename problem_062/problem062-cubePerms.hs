-- Chris Piller
-- problem062-cubePerms.hs

import qualified Data.List as List

-- The number of digits in an integer n
numDigits n = length (show n)

-- All of the positive integers that are cubes
allCubes = [x^3 | x<-[1..]]

-- All of the cubed integers with n or fewer digits
allCubesUpToDigits n = takeWhile (<10^n) allCubes

-- Only the cubes with n digits
cubesWithDigits n = takeWhile (>=10^(n - 1)) (reverse (allCubesUpToDigits n))

-- checks if two numbers are permutations of eachother
arePermutations x y = List.sort (show x) == List.sort (show y)

-- The cube permutations of an integer
cubePermutations n = [x | x<-(cubesWithDigits (numDigits n)), arePermutations x n]

-- The number of permutations that are cubes
numCubePerms n = length (cubePermutations n)

-- Answer
result = take 1 [(x, numCubePerms x) | x<-allCubes, numCubePerms x == 5]

-- print the answer
main = print (result)

-- Testing --------------------------------------------------------------------

-- test using the example value given, should return (41063625,3)
firstThreePermCube = take 1 [(x, numCubePerms x) | x<-allCubes, numCubePerms x == 3]

-- test using the other example values given, returns True if working
sortCubes n = List.sort (cubePermutations n)
samplePermsEqual = 
    (sortCubes 41063625) == (sortCubes 56623104) &&
    (sortCubes 56623104) == (sortCubes 66430125)

-- Testing --------------------------------------------------------------------
