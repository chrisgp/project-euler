from tqdm import tqdm, trange
import numpy as np
import numba
from time import time

start = time()
cap = 1000000


@numba.jit(nopython=True)
def is_prime(n):
    for k in range(2, min(int(np.sqrt(n)) + 3, n)):
        if n % k == 0:
            return False
    return True


@numba.jit(nopython=True)
def totient(n, primes_arr):
    prod = 1.0
    for k in primes_arr:
        if k > min(n, 30):
            break

        if n % k == 0:
            prod *= 1 - (1 / k)

    return int(n * prod + .001)


def get_max(cap, primes_arr):
    max_n = 0
    max_tot = 0
    max_novertot = 0
    for n in range(2, cap + 1):
        tot = totient(n, primes_arr)
        novertot = n / tot
        if novertot > max_novertot:
            print(n, tot, novertot)
            max_n = n
            max_tot = tot
            max_novertot = novertot
    return max_n, max_tot, max_novertot


primes = np.array([n for n in trange(2, cap + 1) if is_prime(n)])
print(get_max(cap, primes))


print(time() - start)
# answer is 510510
