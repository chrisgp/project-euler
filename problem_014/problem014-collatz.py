#!/usr/bin/python

def collatz(start):
    current = start
    length = 1
    while current > 1:
        if current % 2 == 0:
            current /= 2
        else:
            current = current * 3 + 1
        length += 1

    return length

def main():
    maxLen = 0
    maxI = 0

    for i in range(1, 1000000):
        lengthOfI = collatz(i)
        if lengthOfI > maxLen:
            maxLen = lengthOfI
            maxI = i    

    print(maxLen)
    print(maxI)

if __name__ == '__main__':
    main()